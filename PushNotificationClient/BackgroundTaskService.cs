﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace PushNotificationClient
{
    public class BackgroundTaskService
    {
        public BackgroundTaskService(string taskName, string entryPoint)
        {
            _taskName = taskName;
            _entryPoint = entryPoint;
        }

        public async void EnableTask()
        {
            var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            if (backgroundAccessStatus == BackgroundAccessStatus.AlwaysAllowed ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedSubjectToSystemPolicy)
            {
                if (!IsTaskAlreadyRegister())
                {
                    BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                    taskBuilder.Name = _taskName;
                    taskBuilder.TaskEntryPoint = _entryPoint;
                    taskBuilder.SetTrigger(new PushNotificationTrigger());
                    var task = taskBuilder.Register();
                }
            }
        }

        public void DisableTask()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == _taskName)
                {
                    task.Value.Unregister(true);
                }
            }
        }

        private bool IsTaskAlreadyRegister()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == _taskName)
                {
                    return true;
                }
            }

            return false;
        }

        private readonly string _taskName;
        private readonly string _entryPoint;
    }
}
