﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushNotificationClient
{
    public static class Constants
    {
        public const string PushNotifyTaskName = "PushNotifyBackgroundTask";
        public const string PushNotifyEntryPoint = "PushNotifyBackgroundTask.EntryPoint";
    }
}
