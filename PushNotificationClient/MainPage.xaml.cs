﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.PushNotifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PushNotificationClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            _backgroundTaskService = new BackgroundTaskService(Constants.PushNotifyTaskName, Constants.PushNotifyEntryPoint);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter != null)
            {
                UpdateText(e.Parameter.ToString());
            }
            _backgroundTaskService.EnableTask();
        }

        public void UpdateText(string text)
        {
            tb.Text = text;
        }

        private async void btnGetNotificationChannel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
                string uri = channel.Uri;

                // You need to send this uri to your server.
            }

            catch (Exception ex)
            {
                // Could not create a channel. 
            }
        }

        private void btnEnableBackgroundTask_Click(object sender, RoutedEventArgs e)
        {
            _backgroundTaskService.EnableTask();
        }

        private void btnDisableBackgroundTask_Click(object sender, RoutedEventArgs e)
        {
            _backgroundTaskService.DisableTask();
        }

        private readonly BackgroundTaskService _backgroundTaskService;
    }
}
