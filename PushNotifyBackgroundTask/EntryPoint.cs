﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;

namespace PushNotifyBackgroundTask
{
    public sealed class EntryPoint : IBackgroundTask
    {
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var rawNotification = taskInstance.TriggerDetails as RawNotification;
            if (rawNotification != null)
            {
                string text = rawNotification.Content;

                string xml = $@"<toast launch='activated by push notification'>
                                    <visual>
                                        <binding template = 'ToastGeneric'>
                                            <text>{text}</text>
                                        </binding>
                                    </visual>
                                </toast>";
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                ToastNotification notification = new ToastNotification(xmlDoc);
                ToastNotificationManager.CreateToastNotifier().Show(notification);
            }
        }
    }
}
